#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

char get(const int& i, const int& j, const vector<vector<char>>& grid) {
    if(i >= grid.size() || j >= grid[i].size())
        return 'L';

    return grid[i][j];
}


char get_with_offset(int i, int j, const int& offset_i, const int& offset_j, const vector<vector<char>>& grid) {
    while(get(i+offset_i, j+offset_j, grid) == '.') {
        i += offset_i;
        j += offset_j;
    }

    return get(i+offset_i, j+offset_j, grid);
}

int count_occupied_around(const int& i, const int& j, const vector<vector<char>>& grid) {
    return (get_with_offset(i, j, -1, +0, grid) == '#') +
           (get_with_offset(i, j, +1, +0, grid) == '#') +
           (get_with_offset(i, j, -1, -1, grid) == '#') +
           (get_with_offset(i, j, -1, +1, grid) == '#') +
           (get_with_offset(i, j, +1, -1, grid) == '#') +
           (get_with_offset(i, j, +1, +1, grid) == '#') +
           (get_with_offset(i, j, +0, -1, grid) == '#') +
           (get_with_offset(i, j, +0, +1, grid) == '#');
}

int main() {
    ifstream input_file("day11.input");

    vector<vector<char>> grid;
    grid.reserve(16);
    string line;
    while(getline(input_file, line)) {
        vector<char> v;
        v.reserve(line.size());

        for(const char& c : line)
            v.push_back(c);

        grid.push_back(v);
    }

    bool moving = true;
    while(moving) {
        moving = false;
        auto new_grid = grid;

        for(int i = 0; i < grid.size(); ++i) {
            for(int j = 0; j < grid[i].size(); ++j) {
                if(grid[i][j] == 'L' && count_occupied_around(i, j, grid) == 0) {
                    new_grid[i][j] = '#';
                    moving = true;
                } else if(grid[i][j] == '#' && count_occupied_around(i, j, grid) >= 5) {
                    new_grid[i][j] = 'L';
                    moving = true;
                }
            }
        }

        grid = new_grid;
    }

    int ans = 0;
    for(const auto& i : grid)
        for(const char& j : i)
            if(j == '#')
                ans++;

    cout << ans << endl;
}

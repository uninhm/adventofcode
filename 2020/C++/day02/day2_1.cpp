#include<iostream>
#include<fstream>
#include<tuple>

using namespace std;

tuple<int, int, char, string>
parse(string line) {
  int i1 = line.find('-');
  int MIN = stoi(line.substr(0, i1));
  int i2 = line.find(' ');
  int MAX = stoi(line.substr(i1+1, i2));
  char c = line[i2+1];
  i1 = i2+4;
  i2 = line.size();
  string s = line.substr(i1, i2);

  return make_tuple(MIN, MAX, c, s);
}

int main() {
  ifstream input_file("day2.input");

  int ans = 0;

  string line;
  while(getline(input_file, line)) {
    auto t = parse(line);
    int MIN  = get<0>(t);
    int MAX  = get<1>(t);
    char c   = get<2>(t);
    string s = get<3>(t);

    int n = 0; for(char i : s) { if(i == c) n++; }
    if(MIN <= n && n <= MAX)
      ans++;
  }

  cout << ans << endl;
}

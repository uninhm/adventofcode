#include<iostream>
#include<fstream>
#include<tuple>

using namespace std;

tuple<int, int, char, string>
parse(string line) {
  int i1 = line.find('-');
  int index1 = stoi(line.substr(0, i1))-1;
  int i2 = line.find(' ');
  int index2 = stoi(line.substr(i1+1, i2))-1;
  char c = line[i2+1];
  i1 = i2+4;
  i2 = line.size();
  string s = line.substr(i1, i2);

  return make_tuple(index1, index2, c, s);
}

int main() {
  ifstream input_file("day2.input");

  int ans = 0;

  string line;
  while(getline(input_file, line)) {
    auto t = parse(line);
    int i1   = get<0>(t);
    int i2   = get<1>(t);
    char c   = get<2>(t);
    string s = get<3>(t);

    if((s[i1] == c) != (s[i2] == c))
      ans++;
  }

  cout << ans << endl;
}

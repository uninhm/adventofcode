#include<iostream>
#include<fstream>
#include<vector>
#include<tuple>

using namespace std;

int main() {
  ifstream input_file("day8.input");

  string line;
  vector<tuple<string, int, int>> instructions;
  while(getline(input_file, line)) {
      string op = line.substr(0, 3);
      string num = line.substr(4, line.size()-4);
      int arg = stoi(num);
      instructions.push_back(make_tuple(op, arg, 0));
  }
  int acc = 0;
  int i = 0;
  while(true) {
      auto& t = instructions[i];
      string& op    = get<0>(t);
      int&    arg   = get<1>(t);
      int&    count = get<2>(t);

      count++;
      if(count > 1)
          break;

      if(op == "acc")
          acc += arg;
      else if(op == "jmp") {
          i += arg;
          continue;
      }
      i++;
  }

  cout << acc << endl;
}

#include<iostream>
#include<fstream>
#include<vector>
#include<tuple>

using namespace std;

int get_result(vector<tuple<string, int, int>> instructions) {
    int acc = 0;
    int i = 0;
    while(true) {
        if(i == instructions.size())
            break;

        auto& t = instructions[i];
        const string& op    = get<0>(t);
        const int&    arg   = get<1>(t);
        int& count = get<2>(t);

        count++;
        if(count > 1)
            return 1 << 30;

        if(op == "acc")
            acc += arg;
        else if(op == "jmp") {
            i += arg;
            continue;
        }
        i++;
    }

    return acc;
}

int main() {
  ifstream input_file("day8.input");

  string line;
  vector<tuple<string, int, int>> instructions;
  while(getline(input_file, line)) {
      string op = line.substr(0, 3);
      string num = line.substr(4, line.size()-4);
      int arg = stoi(num);
      instructions.push_back(make_tuple(op, arg, 0));
  }

  for(int i = 0; i < instructions.size(); ++i) {
      auto ins = instructions;
      auto& t = ins[i];
      string& op = get<0>(t);

      if(op == "acc")
          continue;

      if(op == "nop")
          op = "jmp";
      else
          op = "nop";

      int result = get_result(ins);
      if(result != 1 << 30)
          cout << result << endl;
  }
}

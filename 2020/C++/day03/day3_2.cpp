#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

int main() {
  vector<string> v;
  v.reserve(400);

  ifstream input_file("day3.input");

  string line;
  while(getline(input_file, line)) {
    v.push_back(line);
  }

  int x[5] = {};
  int trees[5] = {};
  for(int y = 1; y < v.size(); ++y) {
    for(int i = 0; i < 4; ++i) {
      x[i] = (int)((x[i] + 2*i + 1) % v[y].size());

      if(v[y][x[i]] == '#')
        trees[i]++;
    }
  }

  for(int y = 2; y < v.size(); y += 2) {
    x[4] = (int)((x[4] + 1) % v[y].size());

    if(v[y][x[4]] == '#')
      trees[4]++;
  }
  
  int ans = 1;
  for(int t: trees) {
    ans *= t;
    cout << t << " ";
  } cout << endl;

  cout << ans << endl;
}

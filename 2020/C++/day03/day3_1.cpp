#include<iostream>
#include<fstream>

using namespace std;

int main() {
  int right = 3;

  ifstream input_file("day3.input");

  string line;
  getline(input_file, line);
  int x = 0, ans = 0;
  while(getline(input_file, line)) {
    x = (int) ((x + right) % line.size());
    if(line[x] == '#')
      ans++;
  }

  cout << ans << endl;
}

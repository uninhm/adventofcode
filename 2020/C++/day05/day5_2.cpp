#include<iostream>
#include<fstream>
#include<set>

using namespace std;

int main() {
    ifstream input_file("day5.input");

    string line;
    set<int> seats;
    while(getline(input_file, line)) {
        int front = 0,
            back = 128,
            left = 0,
            right = 8,
            row, col;

        string s = line.substr(0, 7);
        for(const char & c: s) {
            if(c == 'F')
                back = (front + back) / 2;
            else if(c == 'B')
                front = (front + back) / 2;
        }
        row = front;

        s = line.substr(7, 3);
        for(const char & c: s) {
            if(c == 'L')
                right = (right + left) / 2;
            else if(c == 'R')
                left = (right + left) / 2;
        }
        col = left;

        int seat_id = row * 8 + col;

        seats.insert(seat_id);
    }

    for(int seat = 1; seat < 1023; ++seat) {
        if( seats.find(seat) == seats.end() &&
            seats.find(seat-1) != seats.end() &&
            seats.find(seat+1) != seats.end()
        ) {
            cout << seat << endl;
            return 0;
        }
    }
}

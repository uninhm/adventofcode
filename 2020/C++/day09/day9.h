#include<iostream>
#include<fstream>
#include<set>
#include<queue>
#include<algorithm>

using namespace std;

bool contains_sum(const set<int>& s, const int& x) {
    return any_of(s.begin(), s.end(),
                  [s, x](int a){ return s.find(x - a) != s.end(); });
}

int get_first_invalid(ifstream& input_file, const int& PREAMBLE_LEN) {
    string line;
    int line_num = 0, n;
    set<int> nums_set;
    queue<int> q;
    while(getline(input_file, line)) {
        n = stoi(line);
        q.push(n);

        if(line_num < PREAMBLE_LEN) {
            nums_set.insert(n);
            line_num++;
            continue;
        }

        if(!contains_sum(nums_set, n))
            break;

        nums_set.erase(q.front());
        q.pop();

        nums_set.insert(n);
        line_num++;
    }

    return n;
}

#include<iostream>
#include"day9.h"

using namespace std;

int main() {
  ifstream input_file("day9.input");
  const int FINDING_NUMBER = get_first_invalid(input_file, 25);
  input_file.seekg(0);

  string line;
  queue<int> q;
  int nums_sum = 0;
  while(getline(input_file, line)) {
      if(FINDING_NUMBER == nums_sum)
          break;

      while(nums_sum > FINDING_NUMBER) {
          nums_sum -= q.front();
          q.pop();
      }

      if(FINDING_NUMBER == nums_sum)
          break;

      int n = stoi(line);
      q.push(n);
      nums_sum += n;
  }

  int a = 1 << 30, b = 0;

  while(q.size()) {
      int x = q.front();
      q.pop();
      a = min(a, x);
      b = max(b, x);
  }

  cout << a + b << endl;
}

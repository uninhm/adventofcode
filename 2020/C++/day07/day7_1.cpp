#include<iostream>
#include<fstream>
#include<map>
#include<set>
#include<utility>

using namespace std;

bool is_digit(const char& c) {
    return '0' <= c && c <= '9';
}

bool is_period_or_comma(const char& c) {
    return c == '.' || c == ',';
}

pair<string, set<string>>
parse_line(const string& line) {
    int contain_index = line.find("contain");
    string color = line.substr(0, contain_index-6);
    set<string> content;
    int last_digit = -1;
    for(int i = contain_index; i < line.size(); ++i) {
        if(is_digit(line[i]))
            last_digit = i;
        else if(is_period_or_comma(line[i]) && last_digit != -1) {
            int subtraction = 6;
            if(line[last_digit] != '1')
                subtraction++;
            content.insert(line.substr(last_digit + 2, i - last_digit - subtraction));
        }
    }

    return make_pair(color, content);
}

map<string, bool> memoization;
bool can_contain_shiny_gold_bag(const string& color, map<string, set<string>>& colors) {
    if(memoization.find(color) != memoization.end())
        return memoization[color];
    if(colors.empty())
        return false;

    auto content = colors[color];
    for(const string& s: content) {
        if(s == "shiny gold" || can_contain_shiny_gold_bag(s, colors)) {
            memoization[color] = true;
            return true;
        }
    }

    memoization[color] = false;
    return false;
}

int main() {
  ifstream input_file("day7.input");

  string line;
  map<string, set<string>> colors;
  int ans = 0;
  while(getline(input_file, line)) {
    auto p = parse_line(line);
    string color = p.first;
    set<string> content = p.second;

    colors[color] = content;
  }

  for(const auto& p: colors) {
      string color = p.first;

      if(can_contain_shiny_gold_bag(color, colors))
          ans++;
  }

  cout << ans << endl;
}

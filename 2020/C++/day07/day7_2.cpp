#include<iostream>
#include<fstream>
#include<map>
#include<set>
#include<utility>

using namespace std;

bool is_digit(const char& c) {
    return '0' <= c && c <= '9';
}

bool is_period_or_comma(const char& c) {
    return c == '.' || c == ',';
}

int ctoi(const char& c) {
    return c - '0';
}

pair <string, set <pair <string, int>>>
parse_line(const string& line) {
    int contain_index = line.find("contain");
    string color = line.substr(0, contain_index-6);
    set<pair<string, int>> content;
    int last_digit = -1;
    for(int i = contain_index; i < line.size(); ++i) {
        if(is_digit(line[i]))
            last_digit = i;
        else if(is_period_or_comma(line[i]) && last_digit != -1) {
            int subtraction = 6;
            if(line[last_digit] != '1')
                subtraction++;
            content.insert(make_pair(line.substr(last_digit + 2, i - last_digit - subtraction), ctoi(line[last_digit])));
        }
    }

    return make_pair(color, content);
}

map<string, int> memoization;
int count_bags(const string& color, map<string, set<pair<string, int>>>& colors) {
    cout << color << endl;
    if(memoization.find(color) != memoization.end())
        return memoization[color];

    auto content = colors[color];
    int result = 1;
    for(const auto& p: content) {
        string s = p.first;
        int n = p.second;
        result += n * count_bags(s, colors);
    }

    memoization[color] = result;
    return result;
}

int main() {
  ifstream input_file("day7.input");

  string line;
  map<string, set<pair<string, int>>> colors;
  while(getline(input_file, line)) {
    auto p = parse_line(line);
    string color = p.first;
    auto content = p.second;

    colors[color] = content;
  }

  cout << count_bags("shiny gold", colors) - 1 << endl;
}

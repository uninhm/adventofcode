#include<iostream>
#include<fstream>
#include<algorithm>
#include<map>

using namespace std;

map<string, string> parse(string line) {
  line += " ";
  map<string, string> m;

  int last_colon = -1;
  int last_space = -1;
  for(int i = 0; i < line.size(); ++i) {
    if(line[i] == ':')
      last_colon = i;
    else if(line[i] == ' ') {
      m[line.substr(last_space+1, 3)] = line.substr(last_colon+1, i-last_colon-1);
      last_space = i;
    }
  }

  return m;
}

int get_field_index(const string& s) {
  string fields[7] = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

  for(int i = 0; i < 7; ++i)
    if(fields[i] == s)
      return i;

  return -1;
}

bool verify_hex(const string& s) {
  if(s.size() != 6)
    return false;

  return all_of(s.begin(), s.end(),
                [](int c){ return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f'); } );
}

bool verify_color(const string& s) {
  string a[7] = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};

  return any_of(begin(a), end(a), [s](const string& e){ return e == s; } );
}

bool verify_number(const string& s) {
  if(s.empty())
    return false;

  return all_of(s.begin(), s.end(),
                [](char c){ return '0' <= c && c <= '9'; } );
}

int main() {
  cout << " byr iyr eyr hgt hcl ecl pid" << endl;
  ifstream input_file("day4.input");

  string line;
  bool verified[7] = {};
  int ans = 0;
  while(getline(input_file, line)) {
    if(line.empty()) {
      bool passed = true;
      for(bool & i : verified) {
        if(!i)
          passed = false;
        cout << "  " << i << " ";
          i = false;
      } cout << endl;
      if(passed)
        ans++;
      continue;
    }

    auto m = parse(line);

    int i;
    for(const auto& p: m) {
      string s = p.first;
      string ms = p.second;
      i = get_field_index(s);
      if(i >= 0) {
        if(s == "byr") {
          if(!verify_number(ms))
            continue;
          int n = stoi(ms);
          if(1920 <= n && n <= 2002)
              verified[i] = true;
        }

        else if(s == "iyr") {
          if(!verify_number(ms))
            continue;
          int n = stoi(ms);
          if(2010 <= n && n <= 2020)
              verified[i] = true;
        }
        
        else if(s == "eyr") {
          if(!verify_number(ms))
            continue;
          int n = stoi(ms);
          if(2020 <= n && n <= 2030)
              verified[i] = true;
        }
        
        else if(s == "hgt") {
          string unit = ms.substr(ms.size() - 2, 2);
          string subs = ms.substr(0, ms.size()-2);

          if(!verify_number(subs) || unit != "cm" && unit != "in")
            continue;
          int n = stoi(subs);
          if(
                  unit == "cm" && 150 <= n && n <= 193 ||
                  unit == "in" && 59 <= n && n <= 76
          )
              verified[i] = true;
        }
        
        else if(s == "hcl") {
          if(ms[0] != '#')
            continue;
          string subs = ms.substr(1, ms.size()-1);
          if(verify_hex(subs))
              verified[i] = true;
        }
        
        else if(s == "ecl") {
          if(verify_color(ms))
              verified[i] = true;
        }

        else if (s == "pid") {
          if(ms.size() == 9 && verify_number(ms))
              verified[i] = true;
        }
      }
    }
  }

  cout << ans << endl;
}

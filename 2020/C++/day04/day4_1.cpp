#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

vector<string> parse(string line) {
  vector<string> v;
  v.reserve(10);

  v.push_back(line.substr(0, 3));
  for(int i = 3; i < line.size(); ++i) {
    if(line[i] == ' ')
      v.push_back(line.substr(i+1, 3));
  }

  return v;
}

int get_field_index(const string& s) {
  string fields[7] = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

  for(int i = 0; i < 7; ++i)
    if(fields[i] == s)
      return i;

  return -1;
}

int main() {
  ifstream input_file("day4.input");

  string line;
  bool verify[7] = {};
  int ans = 0;
  while(getline(input_file, line)) {
    if(line.empty()) {
      bool passed = true;
      for(bool & i : verify) {
        if(!i)
          passed = false;
        i = false;
      }
      if(passed)
        ans++;
      continue;
    }

    auto v = parse(line);

    int i;
    for(const string& s: v) {
      i = get_field_index(s);
      if(i >= 0)
        verify[i] = true;
    }
  }

  cout << ans << endl;
}

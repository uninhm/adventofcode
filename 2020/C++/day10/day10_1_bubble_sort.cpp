#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

void bubble_sort(int* start, int* finish) {
    bool sorted = false;
    while(!sorted) {
        int* p = start;
        int* last = start;
        sorted = true;
        while(p != finish) {
            if(*last > *p) {
                int temp = *last;
                *last = *p;
                *p = temp;

                sorted = false;
            }
            last = p;
            p++;
        }
    }
}

int main() {
  ifstream input_file("day10.input");
  vector<int> v;
  v.reserve(100);

  string line;
  while(getline(input_file, line)) {
    v.push_back(stoi(line));
  }

  bubble_sort(&v[0], &v[v.size()]);

  int diff1count = 0, diff3count = 1;
  int last = 0;
  for(const int& x : v) {
      if(x - last == 1)
          diff1count++;
      else if(x - last == 3)
          diff3count++;
      last = x;
  }

  cout << diff1count << " * " << diff3count << " = " << diff1count * diff3count << endl;
}

#include<iostream>
#include<fstream>
#include<vector>
#include <algorithm>

using namespace std;

int main() {
  ifstream input_file("day10.input");
  vector<int> v;
  v.reserve(100);

  string line;
  while(getline(input_file, line)) {
    v.push_back(stoi(line));
  }

  sort(begin(v), end(v));

  int diff1count = 0, diff3count = 1;
  int last = 0;
  for(const int& x : v) {
      if(x - last == 1)
          diff1count++;
      else if(x - last == 3)
          diff3count++;
      last = x;
  }

  cout << diff1count << " * " << diff3count << " = " << diff1count * diff3count << endl;
}

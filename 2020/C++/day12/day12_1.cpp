#include <iostream>
#include <fstream>

using namespace std;

char left(const char& c) {
    if(c == 'N') return 'W';
    if(c == 'W') return 'S';
    if(c == 'S') return 'E';
    if(c == 'E') return 'N';
    return 'X';
}

char right(const char& c) {
    if(c == 'N') return 'E';
    if(c == 'E') return 'S';
    if(c == 'S') return 'W';
    if(c == 'W') return 'N';
    return 'X';
}

void rotate(char* facing, const char& direction, int degrees) {
    while(degrees) {
        if(direction == 'R')
            *facing = right(*facing);
        else
            *facing = left(*facing);
        degrees -= 90;
    }
}

void move(int* x, int* y, const char& direction, const int& distance) {
    if(direction == 'E')
        *x += distance;
    else if(direction == 'W')
        *x -= distance;
    else if(direction == 'N')
        *y += distance;
    else // (direction == 'S')
        *y -= distance;
}

int main() {
    ifstream input_file("day12.input");

    string line;
    char facing = 'E';
    int x = 0, y = 0;
    while(getline(input_file, line)) {
        int arg = stoi(line.substr(1, line.size()-1));
        if(line[0] == 'R' || line[0] == 'L')
            rotate(&facing, line[0], arg);
        else if(line[0] == 'F')
            move(&x, &y, facing, arg);
        else if(line[0] == 'N' || line[0] == 'S' || line[0] == 'E' || line[0] == 'W')
            move(&x, &y, line[0], arg);
    }

    cout << abs(x) + abs(y) << endl;
}

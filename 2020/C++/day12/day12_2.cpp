#include <iostream>
#include <fstream>

using namespace std;

void rotate(int* x, int* y, const char& direction, int degrees) {
    while(degrees) {
        int tempx = *x;
        if(direction == 'R') {
            *x = *y;
            *y = -tempx;
        } else {
            *x = -(*y);
            *y = tempx;
        }
        degrees -= 90;
    }
}

void move(int* x, int* y, const char& direction, const int& distance) {
    if(direction == 'E')
        *x += distance;
    else if(direction == 'W')
        *x -= distance;
    else if(direction == 'N')
        *y += distance;
    else // (direction == 'S')
        *y -= distance;
}

int main() {
    ifstream input_file("day12.input");

    string line;
    int wx = 10, wy = 1; // waypoint coordinates, relative to ship
    int sx = 0, sy = 0; // ship absolute coordinates
    while(getline(input_file, line)) {
        int arg = stoi(line.substr(1, line.size()-1));
        if(line[0] == 'R' || line[0] == 'L')
            rotate(&wx, &wy, line[0], arg);
        else if(line[0] == 'F') {
            sx += arg * wx;
            sy += arg * wy;
        } else if(line[0] == 'N' || line[0] == 'S' || line[0] == 'E' || line[0] == 'W')
            move(&wx, &wy, line[0], arg);
    }

    cout << abs(sx) + abs(sy) << endl;
}

#include<iostream>
#include<fstream>
#include<vector>
#include<set>

using namespace std;

int main() {
  vector<long> v;
  v.reserve(210);

  ifstream input_file("day1.input");

  string line;
  while(getline(input_file, line)) {
    v.push_back(stoi(line));
  }

  set<long> s;

  int a, b, c;
  for(int i = 0; i < v.size(); ++i) {
    for(int j = i+1; j < v.size(); ++j) {
      a = v[i];
      b = v[j];
      c = 2020 - a - b;

      if(s.find(2020 - v[i] - v[j]) != s.end()) {
        cout << a << " " << b << " " << c << " " << a * b * c << endl;
        return 0;
      }
      s.insert(v[i]);
      s.insert(v[j]);
    }
  }
}

#include<iostream>
#include<fstream>
#include<vector>
#include<set>

using namespace std;

int main() {
  vector<long> v;
  v.reserve(210);

  ifstream input_file("day1.input");

  string line;
  set<long> s;
  long a, b;
  while(getline(input_file, line)) {
    a = stoi(line);
    b = 2020 - a;
    if(s.find(b) != s.end()) {
      cout << a << " " << b << " " << a * b << endl;
      return 0;
    }
    s.insert(a);
  }
}

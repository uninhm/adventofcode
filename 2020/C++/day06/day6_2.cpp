#include<iostream>
#include<fstream>
#include<map>

using namespace std;

int main() {
  ifstream input_file("day6.input");

  string line;
  map<char, int> answers;
  int ans = 0;
  int persons;
  while(getline(input_file, line)) {
    if(line.empty()) {
        for(const auto& p: answers)
            if(p.second == persons)
                ans++;

        answers.clear();
        persons = 0;
        continue;
    }
    for(const char& c: line) {
        if(answers.find(c) == answers.end())
            answers[c] = 0;
        answers[c]++;
    }
    persons++;
  }

  cout << ans << endl;
}

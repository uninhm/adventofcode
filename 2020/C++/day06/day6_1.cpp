#include<iostream>
#include<fstream>
#include<set>

using namespace std;

int main() {
  ifstream input_file("day6.input");

  string line;
  set<char> questions;
  int ans = 0;
  while(getline(input_file, line)) {
    if(line.empty()) {
        ans += questions.size();
        questions.clear();
        continue;
    }
    for(const char& c: line) {
        questions.insert(c);
    }
  }

  cout << ans << endl;
}
